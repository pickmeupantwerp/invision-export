sleepTime = 100;
loop1 = function(data) {
    // Get/update image elements
    data.imgs = $('body > div:nth-child(5) > div > div.screens > div > div > div > a > span.thumbnail-viewport > img');

    // Do loop checks
    data.i++;
    data.n = data.i + 1;
    if (data.i >= data.imgs.length) {
        loopEnd(data);
        return;
    }
    /*else if (data.i > 2) {
        loopEnd(data);
        return;
    }//*/

    console.log('Opening image ' + data.n);

    // Opens current image
    $(data.imgs[data.i]).parent().parent().click();

    setTimeout(() => {
        loop2(data);
    }, sleepTime);
};
loop2 = function(data) {
    // Get element containing large image
    let largeImg = $('#mobile_screen > div > div:nth-child(5) > img');
    // Append url
    data.urls.push(largeImg.prop('src'));
    
    //setTimeout(() => {
        loop3(data);
    //}, sleepTime);
}
loop3 = function(data) {
    console.log('Closing image ' + data.n);
    // Close detail view
    $('body > div:nth-child(3) > div > div.toolbar.highlight-tour > div.tools > div.tool.browse-toggle-tool > a').click();

    setTimeout(() => {
        loop1(data);
    }, sleepTime);
};
loopEnd = function (data) {
    if (data.urls.length == 0) {
        console.warn('No images found. Did you start the script in the image overview? (Not in detail view!)');
    }
    else {
        // Print urls
        console.log(data.urls.join("\n"));
    }
};
getUrls = function() {
    // Start loop
    loop1({
        imgs: [],
        urls: [],
        i: -1
    });
};
getUrls();
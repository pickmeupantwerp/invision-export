import os, re
from docx import Document, shared
from docx.enum.text import WD_BREAK, WD_ALIGN_PARAGRAPH

def deleteFile(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

def renameImages(dir):
    regex = re.compile('^(.+)\.(jpg|png)')

    imgs = []
    for path, dirs, files in os.walk(dir):
        for file in files:
            sch = regex.search(file)
            if (sch is not None):
                newFile = sch.group()
                oldFilePath = os.path.join(path, file)
                newFilePath = os.path.join(path, newFile)

                os.rename(oldFilePath, newFilePath)
                print('Renamed {oldFile} to {newFile}'.format(oldFile=oldFilePath, newFile=newFilePath))

                imgs.append(newFilePath)
    return imgs

def createWordExport(imgs):
    doc = Document()

    # Set margin
    margin = 1
    sections = doc.sections
    for section in sections:
        section.page_width = shared.Cm(21.0)
        section.page_height = shared.Cm(29.7)
        section.top_margin = shared.Cm(margin)
        section.bottom_margin = shared.Cm(margin)
        section.left_margin = shared.Cm(margin)
        section.right_margin = shared.Cm(margin)

    cnt = 0
    length = len(imgs)
    for img in imgs:
        p = doc.add_paragraph()
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        r = p.add_run()
        r.add_picture(img, height=shared.Cm(27.7))
        if cnt + 1 < length:
            r.add_break(WD_BREAK.PAGE)

        cnt+=1
        print('Added image {nr} to doc'.format(nr=cnt))
    
    fileName = 'export.docx'
    deleteFile(fileName)
    doc.save(fileName)

# MAIN
imgs = renameImages('.')
print()
createWordExport(imgs)
